import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";

import { ActivatedRoute } from '@angular/router';
import { AppComponent } from './app.component';
import { ExtensionPipe } from './pipes/extension.pipe';
import { RemoveComponentTagDirective } from './directives/remove-component-tag.directive';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FilesComponent } from './components/files/files.component';
import { NewFileComponent } from './components/new-file/new-file.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AppRoutingModule } from './app-routing.module';
import { DatePipe } from '@angular/common';
import { FilesService } from './files.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FilesComponent,
    ExtensionPipe,
    RemoveComponentTagDirective,
    NewFileComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: 
  [DatePipe,
  {provide: FilesService, useClass: FilesService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
