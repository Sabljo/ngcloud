import { NgModule } from '@angular/core';
import {Route,RouterModule,Routes} from "@angular/router";
import { FilesComponent } from './components/files/files.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NewFileComponent } from './components/new-file/new-file.component';

const routes: Routes=[
  {path: "", pathMatch: "full", redirectTo: "files"},
  {path:"files",component: FilesComponent},
  {path:"profile",component: ProfileComponent},
  {path:"files/new-file",component: NewFileComponent},
  {path: "**", redirectTo: "files"}
];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
]
})
export class AppRoutingModule { }
