import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'extensionPipe'
})
export class ExtensionPipe implements PipeTransform {

  transform(name: string): string {
    var arr: String[]=['jpg','mp3','pdf','ppt','pptx','doc','docx','txt','xls','xlsx'];
    if(arr.indexOf(name.split(".",2)[1])!==-1){
      return "../../../assets/img/"+ name.split(".",2)[1]+".png";
    }else{
      return "../../../assets/img/file.png";
    }
  } 


}
