import { Injectable } from '@angular/core';
import {FileMetadata} from './classes/file-metadata';
import {PaginationInfo} from './classes/pagination-info';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';



@Injectable()
export class FilesService {

  private readonly url = '/api/files';
  public files: BehaviorSubject<FileMetadata[]> = new BehaviorSubject<FileMetadata[]>([]);
  public paginationInfo: BehaviorSubject<PaginationInfo> = 
    	new BehaviorSubject<PaginationInfo>(
        {itemsPerPage: 5,
          currentPage: 0,
         totalItems: 0}
      );
  constructor(private readonly http: HttpClient) {
    this.getAllFiles().subscribe();
    this.getFiles()
    .subscribe((files: FileMetadata[]) => {
      this.files.next(files);
    });
    this.paginationInfo.subscribe();
   }

  postFile(file: FileMetadata): Observable<FileMetadata> {
    return this.http.post<FileMetadata>(this.url, file)
              .pipe(
                 tap(()=>this.paginationInfo.next({
                  ...this.paginationInfo.value,
                  totalItems: this.paginationInfo.getValue().totalItems + 1
                }))
          );
  }
  getFiles(): Observable<FileMetadata[]>{
    let paramss= new HttpParams().set('page',String(this.paginationInfo.getValue().currentPage)).set('limit',String(this.paginationInfo.getValue().itemsPerPage));
    return this.http.get<FileMetadata[]>(this.url, {params:paramss})
      .pipe(
        tap((files: FileMetadata[]) => this.files.next(files))
      );
  }
  getAllFiles(): Observable<FileMetadata[]>{
    let paramss= new HttpParams().set('page',String(0)).set('limit',String(0));
    return this.http.get<FileMetadata[]>(this.url, {params:paramss})
      .pipe(
        tap((files: FileMetadata[]) => this.paginationInfo.next({
          ...this.paginationInfo.value,
          totalItems: files.length
        }))
      );
  }
  removeFile(file: FileMetadata): Observable<FileMetadata>{
    return this.http.delete<FileMetadata>(`${this.url}/${file.id}`)
            .pipe(
                 tap(()=>this.paginationInfo.next({
                ...this.paginationInfo.value,
                  totalItems: this.paginationInfo.getValue().totalItems - 1
                }))
            );
  }

}
