import { Directive, ElementRef } from '@angular/core';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';

@Directive({
  selector: '[appRemoveComponentTag]'
})
export class RemoveComponentTagDirective {
  elem : ElementRef;
  constructor(el: ElementRef) {
    this.elem=el
  }
  ngOnInit() {
      var nativeElem: HTMLElement = this.elem.nativeElement;
      const childElements = this.elem.nativeElement.children;
      var parentElem: HTMLElement = nativeElem.parentElement!;
      for (let child of childElements) {
          parentElem.insertBefore(child, nativeElem);
      }
      parentElem.removeChild(nativeElem);
  }
}
