import { Component, OnInit , OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { FilesService } from 'src/app/files.service';
import { DatePipe } from '@angular/common';
import {FileMetadata} from '../../classes/file-metadata';
@Component({
  selector: 'app-new-file',
  templateUrl: './new-file.component.html',
  styleUrls: ['./new-file.component.scss']
})
export class NewFileComponent implements OnInit {
  
  constructor(
    public datepipe: DatePipe,
    private router: Router,
    public readonly filesService: FilesService
    ) {
   }

  ngOnInit(): void {
  }
  uploadFile(value: string) {
    if(value != ""){
      var randomnum = (Math.random() * (10.00 - 2.00 + 1.00) + 1.00).toFixed(2);
      let date: Date = new Date(); 
      this.datepipe.transform(date, 'M/d/yy, h:mm a')
      let newFile: FileMetadata={
        name: value,
        added : String(this.datepipe.transform(date, 'M/d/yy, h:mm a')),
        size: Number(randomnum)
      }
      this.filesService.postFile(newFile).subscribe(() => this.router.navigate(['/']));
    }
  }

  onClick () {
    this.router.navigate(['/']);
  }
}
