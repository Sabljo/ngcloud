import { Component, Input, OnInit } from '@angular/core';
import {FileMetadata} from '../../classes/file-metadata';
import { Router } from '@angular/router';
import { FilesService } from '../../files.service';
import { PaginationInfo } from 'src/app/classes/pagination-info';
@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  public res : number[]=[];

  options = [
    { value: '5', id: '5' },
    { value: '10', id: '10' },
    { value: '15', id: '15' },
  ];

  public pageinfo: PaginationInfo={
    itemsPerPage: 5,
    currentPage: 0,
    totalItems: 0
  };

  public selected;
  public selectedIndex : number;


  constructor(
      private router: Router, 
      public readonly filesService: FilesService
    ) 
  {
    this.filesService.paginationInfo.subscribe(pageinformation=>this.pageinfo=pageinformation );
    this.selected = this.pageinfo.itemsPerPage;
    this.selectedIndex=this.pageinfo.currentPage+1;
  }


  prevPage(){
    if(this.selectedIndex>1){
      this.selectedIndex=this.selectedIndex-1;
      this.filesService.paginationInfo.next({
        ...this.filesService.paginationInfo.value,
        currentPage: this.selectedIndex-1,
      }
      );
      this.filesService.getFiles().subscribe();
    }
  }
  nextPage(){
    if(this.selectedIndex!==this.res.length){
      this.selectedIndex=this.selectedIndex+1;
      this.filesService.paginationInfo.next({
        ...this.filesService.paginationInfo.value,
        currentPage: this.selectedIndex-1
      }
      );
      this.filesService.getFiles().subscribe();
    } 
  }
  pageClicked(page: number) {
    this.selectedIndex=page;
    this.filesService.paginationInfo.next({
      ...this.filesService.paginationInfo.value,
      currentPage: page-1
    }
    );
    this.filesService.getFiles().subscribe();
}
  getSelectedValue(limit: any) {
    this.selected=limit;
    this.filesService.paginationInfo.next({
                ...this.filesService.paginationInfo.value,
                itemsPerPage: limit
          }
    );
    this.filesService.getFiles().subscribe();
  }
  pages() {
    let i=0;
    if(this.res.length>0){
       i=this.res[this.res.length-1];
    }
    if(i<Math.ceil(this.pageinfo.totalItems / this.pageinfo.itemsPerPage)){
      this.res.push(i+1);
    }else if(i>Math.ceil(this.pageinfo.totalItems / this.pageinfo.itemsPerPage)){
      this.res.pop();
    }
    return this.res;
  }
  onClick () {
    this.router.navigate(['files/new-file']);
  }
  ngOnInit(): void {

  }
  onDeleteClick(file: FileMetadata){
    this.filesService.removeFile(file).subscribe();
    this.filesService.getFiles().subscribe();
  }

}
