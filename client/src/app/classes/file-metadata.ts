

export interface FileMetadata {
    name: string;
    added: string;
    size: number;
     id?: string;
}
