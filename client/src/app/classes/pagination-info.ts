export type PaginationInfo = {
    itemsPerPage: number,
    currentPage: number,
    totalItems: number
}
