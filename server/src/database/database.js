import mongodb from 'mongodb';

const client= await mongodb.MongoClient('mongodb://localhost:27017',{ useUnifiedTopology: true }).connect();
export const database = client.db('examples');