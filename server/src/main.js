

import express from 'express';
import bodyParser from 'body-parser'
import mongodb from 'mongodb';

(async function () {
  const app = express();
  app.use(bodyParser.json());
  let client,database;

  try{
    client= await mongodb.MongoClient('mongodb://localhost:27017',{ useUnifiedTopology: true }).connect();
    database = client.db('examples');
  }catch(e){
    console.log(e);
  }

  const collection= database.collection('files');
  const route = '/api/files';

  function mapToViewModel(file) {
    file.id = file._id;
    delete file._id;
    return file;
  }
  app.post(route, async (req, res) => {
    const file = req.body;
    try{
      await collection.insertOne(file);
    }catch(e){
      console.log(e);
    }
    res.status(201);
    res.send(mapToViewModel(file));
    console.log(await collection.find().toArray());
  });

  app.get(route, async(req, res) => {
    const page=req.query.page;
    const limit=req.query.limit;
    const allfiles=await collection.find().toArray().length;
    const files = await collection.find().skip(Number(page)*Number(limit)).limit(Number(limit)).toArray();
    console.log(files);
    res.send(files.map(mapToViewModel));
  });
  
  app.delete(`${route}/:id`, async (req, res)=> {
      const id = mongodb.ObjectID(req.params.id);
      const deleted= (await collection.deleteOne({_id: id})).deletedCount;
      if (!deleted) {
        res.status(404);
        res.send({ error: 'Not found!' });
        return;
      }
      res.send();
  });
  	app.listen(3000, () => console.log('Listening on port 3000'));
})();

